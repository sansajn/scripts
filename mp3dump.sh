#!/bin/bash

# Pouzitie:
#    /mp3dump.sh "Tomorr*"
# najde vsetky subory zacinajuce na 'Tomorr' a extrahuje audio stream vo formate mp3

for file in $1; do
	ext=${file##*.}
	result_fname=`basename "$file" ".$ext"`.mp3
	avconv -i "$file" -f mp3 -ab 192000 -vn "$result_fname"
done
